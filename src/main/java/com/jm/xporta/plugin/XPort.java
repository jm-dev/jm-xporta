/*
 * Copyright (C) 2013 Michael
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.jm.xporta.plugin;

import com.jm.xporta.exceptions.XPortException;

/**
 * 
 * @param <INPUT>
 * @param <OUTPUT>
 * @created Feb 12, 2013
 * @author Michael L.R. Marques
 */
public interface XPort<INPUT, OUTPUT> {
    
    /**
     * Initializes the plugin with the @code{ResultSet} data
     * This will tell the plugin to start 
     * @param input
     * @param params
     * @return XPortResult
     * @throws XPortException 
     */
    XPortResult initialize(INPUT input, Object... params) throws XPortException;
    
    /**
     * Allows client to exclude plugin
     * @return boolean
     */
    boolean isExcluded();
    
    /**
     * 
     * @return String
     */
    String getName();
    
    /**
     * 
     * @return String
     */
    String getDescription();
    
    /**
     * 
     * @return INPUT
     */
    INPUT getInput();
    
    /**
     * 
     * @return OUTPUT
     */
    OUTPUT getOutput();
    
    /**
     * 
     * @param output
     * @throws XPortException 
     */
    void setOutput(OUTPUT output) throws XPortException;
    
}
