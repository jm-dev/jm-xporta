/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.xporta.plugin;

import java.util.Iterator;

/**
 *
 * @author Michael L.R. Marques
 */
public class XPortResult implements Iterable<String> {
    
    private boolean successful;
    private String[] messages;
    
    /**
     * 
     */
    public XPortResult() {
        this(true, new String[0]);
    }
    
    /**
     * 
     * @param successful
     * @param messages 
     */
    public XPortResult(boolean successful, String... messages) {
        this.successful = successful;
        this.messages = messages;
    }

    /**
     * @return the successful
     */
    public boolean isSuccessful() {
        return successful;
    }

    /**
     * @param successful the successful to set
     */
    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    /**
     * @return the messages
     */
    public String[] getMessages() {
        return messages;
    }

    /**
     * @param messages the messages to set
     */
    public void setMessages(String... messages) {
        this.messages = messages;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Iterator<String> iterator() {
        return new MessageIterator(this.messages);
    }
    
    /**
     * 
     */
    final static class MessageIterator implements Iterator<String> {
        
        private final String[] messages;
        private int index;
        
        /**
         * 
         * @param messages 
         */
        public MessageIterator(String... messages) {
            this.messages = messages;
        }
        
        /**
         * 
         * @return 
         */
        @Override
        public boolean hasNext() {
            return this.messages != null && this.index < this.messages.length;
        }
        
        /**
         * 
         * @return 
         */
        @Override
        public String next() {
            return this.messages[index++];
        }
        
        /**
         * 
         */
        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        
    }
    
}
