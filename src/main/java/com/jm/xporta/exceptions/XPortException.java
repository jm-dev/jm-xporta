/*
 * Copyright (C) 2013 Michael
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.jm.xporta.exceptions;

/**
 *
 * @created Apr 26, 2013
 * @author Michael L.R. Marques
 */
public class XPortException extends Exception {
    
    /**
     * 
     */
    public XPortException() {
        super();
    }
    
    /**
     * 
     * @param message 
     */
    public XPortException(String message) {
        super(message);
    }
    
    /**
     * 
     * @param cause 
     */
    public XPortException(Throwable cause) {
        super(cause);
    }
    
    /**
     * 
     * @param message
     * @param cause 
     */
    public XPortException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
