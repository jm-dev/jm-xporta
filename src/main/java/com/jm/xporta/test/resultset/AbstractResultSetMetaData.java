/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.xporta.test.resultset;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Michael L.R. Marques
 */
public class AbstractResultSetMetaData implements ResultSetMetaData {

    private final List<String> columns;
    private final List<String> columnClassNames;
    private final List<String> columnTypeNames;
    private final List<Integer> columnTypes;

    /**
     *
     */
    public AbstractResultSetMetaData() {
        this.columns = new ArrayList();
        this.columnClassNames = new ArrayList();
        this.columnTypeNames = new ArrayList();
        this.columnTypes = new ArrayList();
    }

    /**
     * 
     * @param column
     * @return 
     * @throws java.sql.SQLException 
     */
    public int getIndex(String column) throws SQLException {
        if (!this.columns.contains(column)) {
            throw new SQLException(column + " does not exist");
        }
        return this.columns.indexOf(column);
    }

    /**
     *
     * @return
     * @throws SQLException
     */
    @Override
    public int getColumnCount() throws SQLException {
        return this.columns.size();
    }

    /**
     *
     * @param column
     * @return
     * @throws SQLException
     */
    @Override
    public String getColumnName(int column) throws SQLException {
        return this.columns.get(column);
    }
    
    /**
     * 
     * @param column
     * @return
     * @throws SQLException 
     */
    @Override
    public String getColumnClassName(int column) throws SQLException {
        return this.columnClassNames.get(column);
    }
    
    /**
     * 
     * @param column
     * @return
     * @throws SQLException 
     */
    @Override
    public int getColumnType(int column) throws SQLException {
        return this.columnTypes.get(column);
    }
    
    /**
     * 
     * @param column
     * @return
     * @throws SQLException 
     */
    @Override
    public String getColumnTypeName(int column) throws SQLException {
        return this.columnTypeNames.get(column);
    }

    /**
     *
     * @param column
     * @return
     * @throws SQLException
     */
    @Override
    public String getColumnLabel(int column) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isAutoIncrement(int column) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isCaseSensitive(int column) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isSearchable(int column) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isCurrency(int column) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int isNullable(int column) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isSigned(int column) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getColumnDisplaySize(int column) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getSchemaName(int column) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getPrecision(int column) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getScale(int column) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getTableName(int column) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getCatalogName(int column) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isReadOnly(int column) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isWritable(int column) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isDefinitelyWritable(int column) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
